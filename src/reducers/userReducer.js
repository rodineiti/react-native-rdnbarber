const initialState = {
  token: '',
  avatar: '',
  favotites: [],
  appointments: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SET_AVATAR':
      return { ...state, avatar: action.payload.avatar };
    case 'SET_TOKEN':
      return { ...state, token: action.payload.token };
    case 'LOGOUT':
      return initialState;
    default:
      return state;
  }
};
