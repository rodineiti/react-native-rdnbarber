import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import styled from 'styled-components/native';

const Modal = styled.Modal``;

const Area = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.5);
  justify-content: flex-end;
`;
const ModalBody = styled.View`
  background-color: #83d6e3;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
  min-height: 500px;
  padding: 10px 20px 40px 20px;
`;

const BtnClose = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
`;

const ModalItem = styled.View`
  background-color: #ffffff;
  border-radius: 10px;
  margin-top: 15px;
  padding: 10px;
`;

const ModalInfo = styled.View`
  flex-direction: row;
  align-items: center;
`;

const ModalInfoService = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const Avatar = styled.Image`
  width: 56px;
  height: 56px;
  border-radius: 20px;
  background-color: #e7e7e7;
  margin-right: 15px;
`;

const Name = styled.Text`
  color: #000000;
  font-size: ${(props) => props.size || 18}px;
  font-weight: bold;
`;

const BtnFinish = styled.TouchableOpacity`
  background-color: #268596;
  height: 60px;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  margin-top: 15px;
`;

const BtnText = styled.Text`
  color: #ffffff;
  font-size: 17px;
  font-weight: bold;
`;

const DateHeader = styled.View`
  flex-direction: row;
`;

const DatePrev = styled.TouchableOpacity`
  flex: 1;
  justify-content: flex-end;
  align-items: flex-end;
`;

const DateArea = styled.View`
  width: 140px;
  justify-content: center;
  align-items: center;
`;

const DateTitle = styled.Text`
  font-size: 17px;
  font-weight: bold;
  color: #000000;
`;

const DateNext = styled.TouchableOpacity`
  flex: 1;
  justify-content: flex-start;
`;

const DateList = styled.ScrollView``;

const DateItem = styled.TouchableOpacity`
  width: 45px;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  padding-top: 5px;
  padding-bottom: 5px;
`;

const DateItemWeekDay = styled.Text`
  font-size: 16px;
  font-weight: bold;
`;

const DateItemWeekNumber = styled.Text`
  font-size: 16px;
  font-weight: bold;
`;

const TimeList = styled.ScrollView``;

const TimeItem = styled.TouchableOpacity`
  width: 75px;
  height: 40px;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
`;

const TimeText = styled.Text`
  font-size: 16px;
  font-weight: bold;
`;

import ExpandSvg from './../../../assets/expand.svg';

const months = [
  'Janeiro',
  'Fevereiro',
  'Março',
  'Abril',
  'Maio',
  'Junho',
  'Julho',
  'Agosto',
  'Setembro',
  'Outubro',
  'Novembro',
  'Dezembro',
];

const days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];

import PrevSvg from './../../../assets/nav_prev.svg';
import NextSvg from './../../../assets/nav_next.svg';
import useApi from '../../services/api';

export default function ModalBarber({ show, setShow, barber, service = null }) {
  const api = useApi();
  const navigation = useNavigation();
  const [year, setYear] = useState(0);
  const [month, setMonth] = useState(0);
  const [day, setDay] = useState(0);
  const [hour, setHour] = useState(null);
  const [listDays, setListDays] = useState([]);
  const [listHours, setListHours] = useState([]);

  useEffect(() => {
    let today = new Date();
    setYear(today.getFullYear());
    setMonth(today.getMonth());
    setDay(today.getDate());
  }, []);

  useEffect(() => {
    if (barber.available && day > 0) {
      let daysInMonth = new Date(year, month + 1, 0).getDate(); // pega o ultimo dia do mês
      let list = [];
      for (let i = 1; i <= daysInMonth; i++) {
        let dt = new Date(year, month, i);
        let y = dt.getFullYear();
        let m = dt.getMonth() + 1;
        let d = dt.getDate();
        let selDate = `${y}-${m
          .toString()
          .padStart(2, '0')}-${d.toString().padStart(2, '0')}`;

        let avalilabity = barber.available.filter((e) => e.date === selDate);

        list.push({
          status: avalilabity.length > 0 ? true : false,
          weekday: days[dt.getDay()],
          number: i,
        });
      }

      setListDays(list);
      setDay(0);
      setListHours([]);
      setHour([]);
    }
  }, [barber, year, month]);

  useEffect(() => {
    if (barber.available && day > 0) {
      let dt = new Date(year, month, day);
      let y = dt.getFullYear();
      let m = dt.getMonth() + 1;
      let d = dt.getDate();
      let selDate = `${y}-${m
        .toString()
        .padStart(2, '0')}-${d.toString().padStart(2, '0')}`;

      let avalilabity = barber.available.filter((e) => e.date === selDate);

      if (avalilabity.length > 0) {
        setListHours(avalilabity[0].hours);
      }
    }
    setHour(null);
  }, [barber, day]);

  const addAppointment = async () => {
    if (
      barber &&
      service &&
      year > 0 &&
      month > 0 &&
      day > 0 &&
      hour !== null
    ) {
      const response = await api.addAppointment(
        barber.id,
        service.id,
        year,
        month + 1,
        day,
        hour.substr(0, 2)
      );

      if (response && response.data) {
        if (!response.data.error) {
          setShow(false);
          navigation.navigate('Appointments');
        } else {
          alert(response.data.message);
        }
      } else {
        alert('Não foi possível agendar, tente novamente');
      }
    } else {
      alert('Você precisa selecionar o dia e a hora');
      return;
    }
  };

  return (
    <Modal visible={show} transparent={true} animationType="slide">
      <Area>
        <ModalBody>
          <BtnClose onPress={() => setShow(false)}>
            <ExpandSvg width="40" height="40" fill="#000000" />
          </BtnClose>

          <ModalItem>
            <ModalInfo>
              <Avatar source={{ uri: barber.avatar_url }} />
              <Name>{barber.name}</Name>
            </ModalInfo>
          </ModalItem>

          {service && (
            <ModalItem>
              <ModalInfoService>
                <Name size={16}>{service.name}</Name>
                <Name size={16}>
                  {service.price.toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL',
                  })}
                </Name>
              </ModalInfoService>
            </ModalItem>
          )}

          <ModalItem>
            <DateHeader>
              <DatePrev
                onPress={() => {
                  let mountDate = new Date(year, month, 1);
                  mountDate.setMonth(mountDate.getMonth() - 1);
                  setYear(mountDate.getFullYear());
                  setMonth(mountDate.getMonth());
                  setDay(0);
                }}
              >
                <PrevSvg width="35" height="35" fill="#000000" />
              </DatePrev>
              <DateArea>
                <DateTitle>
                  {months[month]} {year}
                </DateTitle>
              </DateArea>
              <DateNext
                onPress={() => {
                  let mountDate = new Date(year, month, 1);
                  mountDate.setMonth(mountDate.getMonth() + 1);
                  setYear(mountDate.getFullYear());
                  setMonth(mountDate.getMonth());
                  setDay(0);
                }}
              >
                <NextSvg width="35" height="35" fill="#000000" />
              </DateNext>
            </DateHeader>
            <DateList horizontal={true} showsHorizontalScrollIndicator={false}>
              {listDays.length > 0 &&
                listDays.map((item, key) => (
                  <DateItem
                    key={key}
                    onPress={() => (item.status ? setDay(item.number) : null)}
                    style={{
                      opacity: item.status ? 1 : 0.5,
                      backgroundColor:
                        item.number === day ? '#4eadbe' : '#FFFFFF',
                    }}
                  >
                    <DateItemWeekDay
                      style={{
                        color: item.number === day ? '#FFFFFF' : '#000000',
                      }}
                    >
                      {item.weekday}
                    </DateItemWeekDay>
                    <DateItemWeekNumber
                      style={{
                        color: item.number === day ? '#FFFFFF' : '#000000',
                      }}
                    >
                      {item.number}
                    </DateItemWeekNumber>
                  </DateItem>
                ))}
            </DateList>
          </ModalItem>

          {day > 0 && listHours.length > 0 && (
            <ModalItem>
              <TimeList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                {listHours.map((item, key) => (
                  <TimeItem
                    key={key}
                    onPress={() => setHour(item)}
                    style={{
                      backgroundColor: item === hour ? '#4eadbe' : '#FFFFFF',
                    }}
                  >
                    <TimeText
                      style={{
                        color: item === hour ? '#FFFFFF' : '#000000',
                        fontWeight: item === hour ? 'bold' : 'normal',
                      }}
                    >
                      {item}
                    </TimeText>
                  </TimeItem>
                ))}
              </TimeList>
            </ModalItem>
          )}

          <BtnFinish onPress={addAppointment}>
            <BtnText>Finalizar agendamento</BtnText>
          </BtnFinish>
        </ModalBody>
      </Area>
    </Modal>
  );
}
