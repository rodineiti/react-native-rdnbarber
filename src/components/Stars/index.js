import React from 'react';
import styled from 'styled-components/native';

import StarEmptySvg from './../../../assets/star_empty.svg';
import StarHalfSvg from './../../../assets/star_half.svg';
import StarSvg from './../../../assets/star.svg';

const Area = styled.View`
  flex-direction: row;
`;

const Star = styled.View``;

const TextStar = styled.Text`
  font-size: 12px;
  font-weight: bold;
  margin-left: 5px;
  color: #737373;
`;

export default function Stars({ stars, showNumber }) {
  let st = [0, 0, 0, 0, 0];
  let floor = Math.floor(stars);
  let left = stars - floor;

  for (var i = 0; i < floor; i++) {
    st[i] = 2;
  }

  if (left > 0) {
    st[i] = 1;
  }

  return (
    <Area>
      {st.map((i, k) => (
        <Star key={k}>
          {i === 0 && <StarEmptySvg width="20" height="20" fill="#ff9200" />}
          {i === 1 && <StarHalfSvg width="20" height="20" fill="#ff9200" />}
          {i === 2 && <StarSvg width="20" height="20" fill="#ff9200" />}
        </Star>
      ))}
      {showNumber && <TextStar>{stars}</TextStar>}
    </Area>
  );
}
