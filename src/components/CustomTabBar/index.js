import React from 'react';
import styled from 'styled-components/native';

import HomeSvg from './../../../assets/home.svg';
import SearchSvg from './../../../assets/search.svg';
import TodaySvg from './../../../assets/today.svg';
import AccountSvg from './../../../assets/account.svg';
import FavoriteSvg from './../../../assets/favorite.svg';
import { useSelector } from 'react-redux';

const Area = styled.View`
  height: 60px;
  background-color: #4eadbe;
  flex-direction: row;
`;

const Item = styled.TouchableOpacity`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const ItemCenter = styled.TouchableOpacity`
  width: 70px;
  height: 70px;
  justify-content: center;
  align-items: center;
  background-color: #ffffff;
  border-radius: 35px;
  border: 3px solid #4eadbe;
  margin-top: -20px;
`;

const Avatar = styled.Image`
  width: 30px;
  height: 30px;
  border-radius: 15px;
`;

export default function CustomTabBar({ state, navigation }) {
  const avatar = useSelector((state) => state.userReducer.avatar);
  return (
    <Area>
      <Item onPress={() => navigation.navigate('Home')}>
        <HomeSvg
          style={{ opacity: state.index === 0 ? 1 : 0.5 }}
          width="30"
          height="30"
          fill="#ffffff"
        />
      </Item>
      <Item onPress={() => navigation.navigate('Search')}>
        <SearchSvg
          style={{ opacity: state.index === 1 ? 1 : 0.5 }}
          width="30"
          height="30"
          fill="#ffffff"
        />
      </Item>
      <ItemCenter onPress={() => navigation.navigate('Appointments')}>
        <TodaySvg width="30" height="30" fill="#4eadbe" />
      </ItemCenter>
      <Item onPress={() => navigation.navigate('Favorites')}>
        <FavoriteSvg
          style={{ opacity: state.index === 3 ? 1 : 0.5 }}
          width="30"
          height="30"
          fill="#ffffff"
        />
      </Item>
      <Item onPress={() => navigation.navigate('Profile')}>
        {avatar !== '' ? (
          <Avatar
            source={{ uri: avatar }}
            style={{ opacity: state.index === 4 ? 1 : 0.5 }}
          />
        ) : (
          <AccountSvg
            style={{ opacity: state.index === 4 ? 1 : 0.5 }}
            width="30"
            height="30"
            fill="#ffffff"
          />
        )}
      </Item>
    </Area>
  );
}
