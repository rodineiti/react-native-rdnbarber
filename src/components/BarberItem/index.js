import React from 'react';
import { useNavigation } from '@react-navigation/native';
import styled from 'styled-components/native';
import Stars from '../Stars';

const Area = styled.TouchableOpacity`
  background-color: #ffffff;
  margin-bottom: 20px;
  border-radius: 20px;
  padding: 15px;
  flex-direction: row;
`;

const Avatar = styled.Image`
  width: 88px;
  height: 88px;
  border-radius: 20px;
`;

const InfoArea = styled.View`
  margin-left: 20px;
  justify-content: space-between;
`;

const Name = styled.Text`
  font-size: 17px;
  font-weight: bold;
`;

const BtnProfile = styled.View`
  width: 85px;
  height: 26px;
  border: 1px solid #4edabe;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
`;

const BtnText = styled.Text`
  font-size: 17px;
  color: #268596;
`;

export default function BarberItem({ data }) {
  const navigation = useNavigation();
  return (
    <Area
      onPress={() =>
        navigation.navigate('Barber', {
          user: data,
        })
      }
    >
      <Avatar source={{ uri: data.avatar_url }} />
      <InfoArea>
        <Name>{data.name}</Name>
        <Stars stars={data.starts} showNumber={true} />
        <BtnProfile>
          <BtnText>Ver perfil</BtnText>
        </BtnProfile>
      </InfoArea>
    </Area>
  );
}
