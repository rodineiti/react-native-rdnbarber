import React from 'react';
import styled from 'styled-components/native';

const Area = styled.View`
  background-color: #ffffff;
  margin-bottom: 20px;
  border-radius: 20px;
  padding: 15px;
`;

const UserArea = styled.View`
  flex: 1;
  flex-direction: row;
`;

const Avatar = styled.Image`
  width: 56px;
  height: 56px;
  border-radius: 20px;
  margin-right: 20px;
`;

const Name = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: #000000;
`;

const SplitArea = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 10px;
`;

const ServiceText = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #000000;
`;

const DateArea = styled.View`
  padding: 10px 15px;
  border-radius: 10px;
  background-color: #4eadbe;
`;

const DateText = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #ffffff;
`;

export default function AppointmentItem({ data }) {
  let d = data.appointment_datetime.split(' ');
  let time = d[1].split(':');
  let timeFormat = `${time[0]}:${time[1]}`;
  let date = new Date(d[0]);
  let year = date.getFullYear();
  let month = date.getMonth();
  let day = date.getDate();
  let dateFormat = `${day
    .toString()
    .padStart(2, '0')}/${month.toString().padStart(2, '0')}/${year}`;
  return (
    <Area>
      <UserArea>
        <Avatar source={{ uri: data.barber.avatar_url }} />
        <Name>{data.barber.name}</Name>
      </UserArea>
      <SplitArea>
        <ServiceText>{data.service.name}</ServiceText>
        <ServiceText>
          {data.service.price.toLocaleString('pt-br', {
            style: 'currency',
            currency: 'BRL',
          })}
        </ServiceText>
      </SplitArea>
      <SplitArea>
        <DateArea>
          <DateText>{dateFormat}</DateText>
        </DateArea>
        <DateArea>
          <DateText>{timeFormat}</DateText>
        </DateArea>
      </SplitArea>
    </Area>
  );
}
