import React from 'react';
import styled from 'styled-components/native';

const Area = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  justify-content: center;
  align-items: center;
`;

const Active = styled.ActivityIndicator``;

export default function Loading() {
  return (
    <Area>
      <Active size="large" color="#FFFFFF" />
    </Area>
  );
}
