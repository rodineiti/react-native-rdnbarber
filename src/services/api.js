import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const api = axios.create({
  baseURL: 'http://192.168.1.34:8000/api/v1',
});

api.interceptors.request.use(
  async (config) => {
    let token = null;
    const rootBarber = await AsyncStorage.getItem('persist:root-barber');

    if (rootBarber) {
      const { userReducer } = JSON.parse(rootBarber);
      token = JSON.parse(userReducer).token;
    }

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (err) => {
    return Promise.reject(err);
  }
);

const axiosApi = {
  login: async (email, password) => {
    try {
      const response = await api.post('/auth/login', {
        email,
        password,
      });
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  register: async (name, email, password, password_confirmation) => {
    try {
      const response = await api.post('/auth/register', {
        name,
        email,
        password,
        password_confirmation,
      });
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  me: async () => {
    try {
      const response = await api.get('/auth/me');
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  updateUser: async (formData) => {
    try {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
          Accept: 'application/json',
        },
      };
      const response = await api.post('/auth/updateProfile', formData, config);
      return response;
    } catch (error) {
      console.error(error);
      return error;
    }
  },
  logout: async () => {
    try {
      const response = await api.get('/auth/logout');
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  getBarbers: async (latitude = null, longitude = null, address = '') => {
    try {
      const response = await api.get(
        `/barbers?latitude=${latitude}&longitude=${longitude}&city=${address}`
      );
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  getBarber: async (id) => {
    try {
      const response = await api.get(`/barbers/${id}`);
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  addFavorite: async (barber_id) => {
    try {
      const response = await api.post(`/users/favorite`, { barber_id });
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  getFavorites: async () => {
    try {
      const response = await api.get(`/users/favorites`);
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  getAppointments: async () => {
    try {
      const response = await api.get(`/users/appointments`);
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  addAppointment: async (
    barber_id,
    barber_service_id,
    year,
    month,
    day,
    hour
  ) => {
    try {
      const response = await api.post(`/barbers/${barber_id}/appointment`, {
        barber_service_id,
        year,
        month,
        day,
        hour,
      });
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
  search: async (q) => {
    try {
      const response = await api.get(`/search?q=${q}`);
      return response;
    } catch (error) {
      console.log(error);
      return error;
    }
  },
};

export default () => axiosApi;
