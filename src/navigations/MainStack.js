import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import Preload from './../screens/Preload';
import Login from './../screens/Login';
import Register from './../screens/Register';
import Barber from './../screens/Barber';
import AppTab from './AppTab';

const Stack = createStackNavigator();

export default function MainStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Preload"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="Preload" component={Preload} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="AppTab" component={AppTab} />
        <Stack.Screen name="Barber" component={Barber} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
