import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Container, Loading } from './styled';

import BarberLogo from './../../../assets/barber.svg';

import useApi from '../../services/api';

export default function Preload({ navigation }) {
  const api = useApi();
  const dispatch = useDispatch();
  const token = useSelector((state) => state.userReducer.token);

  useEffect(() => {
    const check = async () => {
      if (!token) {
        navigation.reset({
          routes: [{ name: 'Login' }],
        });
      } else {
        const response = await api.me();
        if (response && response.data) {
          dispatch({
            type: 'SET_AVATAR',
            payload: { avatar: response.data.avatar_url },
          });

          navigation.reset({
            routes: [{ name: 'AppTab' }],
          });
        } else {
          navigation.reset({
            routes: [{ name: 'Login' }],
          });
        }
      }
    };

    check();
  }, [token]);

  return (
    <Container>
      <BarberLogo width="100%" height="160" />
      <Loading size="large" color="#FFFFFF" />
    </Container>
  );
}
