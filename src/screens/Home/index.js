import React, { useState, useEffect } from 'react';
import { RefreshControl } from 'react-native';
import * as Location from 'expo-location';

import {
  Container,
  Scroller,
  Header,
  HeaderTitle,
  SearchButton,
  Area,
  Input,
  Finder,
  ListContainer,
} from './styled';

import Loading from './../../components/Loading';
import BarberItem from './../../components/BarberItem';

import SearchSvg from './../../../assets/search.svg';
import LocationSvg from './../../../assets/my_location.svg';

import useApi from '../../services/api';

export default function Home({ navigation }) {
  const api = useApi();
  const [inputText, setInputText] = useState('');
  const [coords, setCoords] = useState(null);
  const [barbers, setBarbers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    getBarbers();
  }, []);

  const handleGetLocation = async () => {
    setCoords(null);
    let { status } = await Location.requestPermissionsAsync();
    if (status !== 'granted') {
      alert('Permission to access location was denied');
      return;
    }

    let { coords } = await Location.getCurrentPositionAsync({});
    setCoords(coords);
    getBarbers();
  };

  const getBarbers = async () => {
    setLoading(true);
    setInputText('');
    setBarbers([]);

    let latitude = null;
    let longitude = null;

    if (coords) {
      latitude = coords.latitude;
      longitude = coords.longitude;
    }

    const response = await api.getBarbers(latitude, longitude, inputText);
    if (response && response.data) {
      const barbersData = response.data.data;
      if (barbersData.data[0].loc) {
        setInputText(barbersData.data[0].loc);
      }
      setBarbers(barbersData.data);
    }
    setLoading(false);
  };

  const onRefresh = () => {
    setRefreshing(false);
    getBarbers();
  };

  const handleSearch = () => {
    setCoords({});
    getBarbers();
  };

  return (
    <Container>
      <Scroller
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <Header>
          <HeaderTitle numberOfLines={2}>Encontre o seu barbeiro</HeaderTitle>
          <SearchButton onPress={() => navigation.navigate('Search')}>
            <SearchSvg widht="26" height="26" fill="#ffffff" />
          </SearchButton>
        </Header>

        <Area>
          <Input
            value={inputText}
            onChangeText={(t) => setInputText(t)}
            placeholder="Onde você está?"
            placeholderTextColor="#ffffff"
            onEndEditing={handleSearch}
          />
          <Finder onPress={handleGetLocation}>
            <LocationSvg widht="24" height="24" fill="#ffffff" />
          </Finder>
        </Area>

        <ListContainer>
          {barbers.length > 0 &&
            barbers.map((item, key) => <BarberItem key={key} data={item} />)}
        </ListContainer>
      </Scroller>
      {loading && <Loading />}
    </Container>
  );
}
