import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Container,
  Area,
  CustomButton,
  TextSubmit,
  CustomButtonMessage,
  TextAccount,
  Bold,
} from './styled';

import CustomInput from '../../components/CustomInput';
import Loading from '../../components/Loading';

import BarberLogo from './../../../assets/barber.svg';
import EmailSvg from './../../../assets/email.svg';
import LockSvg from './../../../assets/lock.svg';

import useApi from '../../services/api';

export default function Login({ navigation }) {
  const api = useApi();
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  onSubmit = async () => {
    setLoading(true);

    if (email && password) {
      const response = await api.login(email, password);
      if (response && response.data) {
        const { user, access_token } = response.data;

        dispatch({
          type: 'SET_TOKEN',
          payload: { token: access_token },
        });

        dispatch({
          type: 'SET_AVATAR',
          payload: { avatar: user.avatar_url },
        });

        navigation.reset({
          routes: [{ name: 'AppTab' }],
        });
      } else {
        alert('Ocorreu um erro, tente novamente');
      }
    } else {
      alert('Preencha suas crendenciais');
    }

    setLoading(false);
  };

  return (
    <Container>
      <BarberLogo width="100%" height="160" />
      <Area>
        <CustomInput
          iconSvg={EmailSvg}
          placeholder="Digite seu e-mail"
          value={email}
          onChangeText={(t) => setEmail(t)}
          keyboardType="email-address"
        />
        <CustomInput
          iconSvg={LockSvg}
          placeholder="Digite sua senha"
          value={password}
          onChangeText={(t) => setPassword(t)}
          secureTextEntry
        />
        <CustomButton onPress={onSubmit}>
          <TextSubmit>Login</TextSubmit>
        </CustomButton>
      </Area>

      <CustomButtonMessage
        onPress={() =>
          navigation.reset({
            routes: [{ name: 'Register' }],
          })
        }
      >
        <TextAccount>Ainda não possui uma conta?</TextAccount>
        <Bold>Cadastre-se</Bold>
      </CustomButtonMessage>

      {loading && <Loading />}
    </Container>
  );
}
