import React, { useState, useEffect } from 'react';
import { RefreshControl } from 'react-native';
import { Container, Scroller, ListContainer, TextEmpty } from './styled';
import Loading from './../../components/Loading';
import AppointmentItem from './../../components/AppointmentItem';
import useApi from '../../services/api';

export default function Appointments() {
  const api = useApi();
  const [barbers, setBarbers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    getAppointments();
  }, [api]);

  const onRefresh = () => {
    setRefreshing(false);
    getAppointments();
  };

  const getAppointments = async () => {
    setLoading(true);
    setBarbers([]);
    const response = await api.getAppointments();
    if (response && response.data) {
      const barbersData = response.data.data;
      setBarbers(barbersData);
    }
    setLoading(false);
  };

  return (
    <Container>
      <Scroller
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {barbers.length === 0 && (
          <TextEmpty>Você não possui agendamentos</TextEmpty>
        )}
        <ListContainer>
          {barbers.length > 0 &&
            barbers.map((item, key) => {
              return <AppointmentItem key={key} data={item} />;
            })}
        </ListContainer>
      </Scroller>
      {loading && <Loading />}
    </Container>
  );
}
