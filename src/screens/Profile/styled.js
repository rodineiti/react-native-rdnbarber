import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  background-color: #63c2d1;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Area = styled.View`
  width: 100%;
  padding: 20px;
`;

export const CustomButton = styled.TouchableOpacity`
  height: 60px;
  background-color: ${(props) => props.bgcolor || '#268596'};
  border-radius: 30px;
  justify-content: center;
  align-items: center;
`;

export const TextSubmit = styled.Text`
  font-size: 18px;
  color: #fff;
`;

export const Text = styled.Text`
  font-size: 18px;
  color: #ffffff;
  font-weight: bold;
  margin-bottom: 10px;
`;

export const Bold = styled.Text`
  font-size: 16px;
  color: #268596;
  font-weight: bold;
  margin-left: 5px;
`;

export const Avatar = styled.Image`
  width: 88px;
  height: 88px;
  border-radius: 20px;
`;

export const BtnUpload = styled.TouchableOpacity``;
