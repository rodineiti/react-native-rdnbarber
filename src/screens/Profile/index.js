import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import * as ImagePicker from 'expo-image-picker';
import {
  Container,
  Area,
  CustomButton,
  TextSubmit,
  Text,
  Avatar,
  BtnUpload,
} from './styled';

import CustomInput from '../../components/CustomInput';
import Loading from '../../components/Loading';

import PersonLogo from './../../../assets/person.svg';
import EmailSvg from './../../../assets/email.svg';
import LockSvg from './../../../assets/lock.svg';

import useApi from '../../services/api';

export default function Profile({ navigation }) {
  const api = useApi();
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [avatarUrl, setAvatarUrl] = useState(null);
  const [file, setFile] = useState(null);
  const [password, setPassword] = useState('');
  const [password_confirmation, setPasswordConfirmation] = useState('');
  const [loading, setLoading] = useState(false);

  logout = async () => {
    await api.logout();

    dispatch({
      type: 'LOGOUT',
    });

    navigation.reset({
      routes: [{ name: 'Login' }],
    });
  };

  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const {
          status,
        } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  useEffect(() => {
    const getUser = async () => {
      setLoading(true);
      const response = await api.me();
      if (response && response.data) {
        const { name, email, avatar_url } = response.data;
        setName(name);
        setEmail(email);
        setAvatarUrl(avatar_url);
      } else {
        alert('Não foi possível carregar os dados, faça login novamente');
        navigation.reset({
          routes: [{ name: 'Login' }],
        });
      }
      setLoading(false);
    };
    getUser();
  }, [api]);

  onSubmit = async () => {
    setLoading(true);

    if (name && email) {
      const formData = new FormData();
      formData.append('name', name);
      formData.append('email', email);

      if (file) {
        let localUri = file.uri;
        let filename = localUri.split('/').pop();

        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

        formData.append('avatar', { uri: localUri, name: filename, type });
      }
      const response = await api.updateUser(formData);
      if (response && response.data && !response.data.error) {
        alert('Dados atualizados');
        navigation.reset({
          routes: [{ name: 'AppTab' }],
        });
      }
    } else {
      alert('Preencha todos os campos');
    }
    setFile(null);
    setLoading(false);
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setFile(result);
      setAvatarUrl(result.uri);
    }
  };

  return (
    <Container>
      <Text>Atualizar cadastro</Text>
      <BtnUpload onPress={pickImage}>
        {avatarUrl && <Avatar source={{ uri: avatarUrl }} />}
      </BtnUpload>
      <Area>
        <CustomInput
          iconSvg={PersonLogo}
          placeholder="Digite seu nome"
          value={name}
          onChangeText={(t) => setName(t)}
        />
        <CustomInput
          iconSvg={EmailSvg}
          placeholder="Digite seu e-mail"
          value={email}
          onChangeText={(t) => setEmail(t)}
          keyboardType="email-address"
        />
        <CustomInput
          iconSvg={LockSvg}
          placeholder="Digite sua senha"
          value={password}
          onChangeText={(t) => setPassword(t)}
          secureTextEntry
        />
        <CustomInput
          iconSvg={LockSvg}
          placeholder="Confirme sua senha"
          value={password_confirmation}
          onChangeText={(t) => setPasswordConfirmation(t)}
          secureTextEntry
        />
        <CustomButton onPress={onSubmit}>
          <TextSubmit>Atualizar</TextSubmit>
        </CustomButton>

        <CustomButton
          style={{ marginTop: 10 }}
          bgcolor="#ff0000"
          onPress={logout}
        >
          <TextSubmit>Sair</TextSubmit>
        </CustomButton>
      </Area>

      {loading && <Loading />}
    </Container>
  );
}
