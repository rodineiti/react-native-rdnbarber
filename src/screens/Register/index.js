import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Container,
  Area,
  CustomButton,
  TextSubmit,
  CustomButtonMessage,
  TextAccount,
  Bold,
} from './styled';

import CustomInput from '../../components/CustomInput';
import Loading from '../../components/Loading';

import BarberLogo from './../../../assets/barber.svg';
import PersonLogo from './../../../assets/person.svg';
import EmailSvg from './../../../assets/email.svg';
import LockSvg from './../../../assets/lock.svg';

import useApi from '../../services/api';

export default function Login({ navigation }) {
  const api = useApi();
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [password_confirmation, setPasswordConfirmation] = useState('');
  const [loading, setLoading] = useState(false);

  onSubmit = async () => {
    setLoading(true);

    if (name && email && password && password_confirmation) {
      const response = await api.register(
        name,
        email,
        password,
        password_confirmation
      );
      if (response && response.data) {
        const { user, access_token } = response.data;

        dispatch({
          type: 'SET_TOKEN',
          payload: { token: access_token },
        });

        dispatch({
          type: 'SET_AVATAR',
          payload: { avatar: user.avatar_url },
        });

        navigation.reset({
          routes: [{ name: 'AppTab' }],
        });
      }
    } else {
      alert('Preencha todos os campos');
    }

    setLoading(false);
  };

  return (
    <Container>
      <BarberLogo width="100%" height="160" />
      <Area>
        <CustomInput
          iconSvg={PersonLogo}
          placeholder="Digite seu nome"
          value={name}
          onChangeText={(t) => setName(t)}
        />
        <CustomInput
          iconSvg={EmailSvg}
          placeholder="Digite seu e-mail"
          value={email}
          onChangeText={(t) => setEmail(t)}
          keyboardType="email-address"
        />
        <CustomInput
          iconSvg={LockSvg}
          placeholder="Digite sua senha"
          value={password}
          onChangeText={(t) => setPassword(t)}
          secureTextEntry
        />
        <CustomInput
          iconSvg={LockSvg}
          placeholder="Confirme sua senha"
          value={password_confirmation}
          onChangeText={(t) => setPasswordConfirmation(t)}
          secureTextEntry
        />
        <CustomButton onPress={onSubmit}>
          <TextSubmit>Cadastrar</TextSubmit>
        </CustomButton>
      </Area>

      <CustomButtonMessage
        onPress={() =>
          navigation.reset({
            routes: [{ name: 'Login' }],
          })
        }
      >
        <TextAccount>Já possui uma conta?</TextAccount>
        <Bold>Fazer login</Bold>
      </CustomButtonMessage>

      {loading && <Loading />}
    </Container>
  );
}
