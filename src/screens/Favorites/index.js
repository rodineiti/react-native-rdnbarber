import React, { useState, useEffect } from 'react';
import { RefreshControl } from 'react-native';
import {
  Container,
  Scroller,
  ListContainer,
  Header,
  Text,
  TextEmpty,
} from './styled';
import Loading from './../../components/Loading';
import BarberItem from './../../components/BarberItem';
import useApi from '../../services/api';

export default function Favorites() {
  const api = useApi();
  const [barbers, setBarbers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    getFavorites();
  }, [api]);

  const onRefresh = () => {
    setRefreshing(false);
    getFavorites();
  };

  const getFavorites = async () => {
    setLoading(true);
    setBarbers([]);
    const response = await api.getFavorites();
    if (response && response.data) {
      const barbersData = response.data.data;
      setBarbers(barbersData);
    }
    setLoading(false);
  };

  return (
    <Container>
      <Header>
        <Text size={18} bold={true}>
          Favoritos
        </Text>
      </Header>
      <Scroller
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {barbers.length === 0 && (
          <TextEmpty>Você não possui favoritos</TextEmpty>
        )}
        <ListContainer>
          {barbers.length > 0 &&
            barbers.map((item, key) => {
              const { barber } = item;
              return <BarberItem key={key} data={barber} />;
            })}
        </ListContainer>
      </Scroller>
      {loading && <Loading />}
    </Container>
  );
}
