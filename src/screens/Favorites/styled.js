import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #63c2d1;
`;

export const Scroller = styled.ScrollView`
  flex: 1;
  padding: 0 20px;
`;

export const ListContainer = styled.View`
  margin-bottom: 30px;
`;

export const Header = styled.View`
  height: 40px;
  justify-content: center;
  padding: 0 20px;
`;

export const Text = styled.Text`
  text-align: left;
  color: #ffffff;
  font-size: ${(props) => props.size || 14}px;
  font-weight: ${(props) => (props.bold ? 'bold' : 'normal')};
`;

export const TextEmpty = styled.Text`
  text-align: center;
  color: #ffffff;
  margin-top: 30px;
  font-size: ${(props) => props.size || 14}px;
  font-weight: ${(props) => (props.bold ? 'bold' : 'normal')};
`;
