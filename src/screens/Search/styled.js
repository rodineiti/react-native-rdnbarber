import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  background-color: #63c2d1;
  flex: 1;
`;

export const SearchArea = styled.View`
  background-color: #4eadbe;
  height: 40px;
  border-radius: 20px;
  padding: 0px 20px;
  margin: 20px;
  margin-bottom: 0;
`;

export const Input = styled.TextInput`
  flex: 1;
  font-size: 16px;
  color: #ffffff;
`;

export const Scroller = styled.ScrollView`
  flex: 1;
  padding: 20px;
`;

export const ListContainer = styled.View`
  margin-bottom: 30px;
`;

export const Text = styled.Text`
  text-align: center;
  margin-top: 30px;
  color: #ffffff;
  font-size: 14px;
`;
