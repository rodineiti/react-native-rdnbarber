import React, { useState } from 'react';

import {
  Container,
  SearchArea,
  Input,
  Scroller,
  ListContainer,
  Text,
} from './styled';
import Loading from './../../components/Loading';
import BarberItem from './../../components/BarberItem';
import useApi from '../../services/api';

export default function Search() {
  const api = useApi();
  const [term, setTerm] = useState('');
  const [barbers, setBarbers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [noResults, setNoResults] = useState(false);

  const search = async () => {
    if (term !== '') {
      setNoResults(false);
      setLoading(true);
      setBarbers([]);

      const response = await api.search(term);
      if (response && response.data) {
        const barbersData = response.data.data;
        if (barbersData.data.length > 0) {
          setBarbers(barbersData.data);
        } else {
          setNoResults(true);
        }
      } else {
        alert('Nenhum resultado encontrado para sua pesquisa');
      }
      setLoading(false);
    }
  };

  return (
    <Container>
      <SearchArea>
        <Input
          placeholder="Digite o nome do barbeiro"
          placeholderTextColor="#FFFFFF"
          value={term}
          onChangeText={(t) => setTerm(t)}
          onEndEditing={search}
          returnKeyType="search"
          autoFocus
          selectTextOnFocus
        />
      </SearchArea>

      <Scroller>
        {noResults && (
          <Text>Nenhum registro encontrado com o nome "{term}"</Text>
        )}
        <ListContainer>
          {barbers.length > 0 &&
            barbers.map((item, key) => <BarberItem key={key} data={item} />)}
        </ListContainer>
      </Scroller>
      {loading && <Loading />}
    </Container>
  );
}
