import React, { useState, useEffect } from 'react';
import { StatusBar } from 'react-native';
import Swiper from 'react-native-swiper';
import useApi from '../../services/api';

import {
  Container,
  Scroller,
  FakeSwiper,
  BodyArea,
  UserArea,
  ServiceArea,
  ServicesTitle,
  ServiceItem,
  ServiceLeft,
  ServiceName,
  ServicePrice,
  ServiceRightBtn,
  ServiceBtnText,
  CommentArea,
  CommentItem,
  CommentInfo,
  CommentName,
  CommentBodyText,
  SwipeDot,
  SwipeItem,
  SwipeImage,
  UserAvatar,
  UserInfo,
  UserName,
  UserFavBtn,
  Back,
} from './styled';
import Loading from './../../components/Loading';
import Stars from './../../components/Stars';
import ModalBaber from './../../components/ModalBaber';
import FavoriteSvg from './../../../assets/favorite.svg';
import FavoriteFullSvg from './../../../assets/favorite_full.svg';
import BackSvg from './../../../assets/back.svg';
import PrevSvg from './../../../assets/nav_prev.svg';
import NextSvg from './../../../assets/nav_next.svg';

export default function Barber({ route, navigation }) {
  const api = useApi();
  const [loading, setLoading] = useState(false);
  const [favorited, setFavorited] = useState(false);
  const [user, setUser] = useState(route.params.user);
  const [modal, setModal] = useState(false);
  const [serviceSeleted, setServiceSeleted] = useState(null);

  useEffect(() => {
    const getBarber = async () => {
      setLoading(true);
      const response = await api.getBarber(user.id);

      if (response && response.data) {
        const user = response.data.data;
        setUser(user);
        setFavorited(user.favorited);
      } else {
        alert('Não foi possível pegar os dados do usuário');
      }

      setLoading(false);
    };
    getBarber();
  }, []);

  const handleFavorite = async () => {
    await api.addFavorite(user.id);
    setFavorited(!favorited);
  };

  const handleSelected = (item) => {
    setServiceSeleted(item);
    setModal(true);
  };

  return (
    <Container>
      <StatusBar barStyle="dark-content" />
      <Scroller>
        {user.photos && user.photos.length > 0 ? (
          <Swiper
            style={{ height: 240 }}
            dot={<SwipeDot />}
            activeDot={<SwipeDot bgColor="#000000" />}
            paginationStyle={{
              top: 15,
              right: 15,
              bottom: null,
              left: null,
            }}
            autoplay={true}
          >
            {user.photos.map((item, key) => (
              <SwipeItem key={key}>
                <SwipeImage
                  source={{ uri: item.image_url }}
                  resizeMode="cover"
                />
              </SwipeItem>
            ))}
          </Swiper>
        ) : (
          <FakeSwiper></FakeSwiper>
        )}
        <BodyArea>
          <UserArea>
            <UserAvatar source={{ uri: user.avatar_url }} />
            <UserInfo>
              <UserName>{user.name}</UserName>
              <Stars stars={user.starts} showNumber={true} />
            </UserInfo>
            <UserFavBtn onPress={handleFavorite}>
              {favorited ? (
                <FavoriteFullSvg width="24" height="24" fill="#FF0000" />
              ) : (
                <FavoriteSvg width="24" height="24" fill="#FF0000" />
              )}
            </UserFavBtn>
          </UserArea>
          <ServiceArea>
            <ServicesTitle>Lista de serviços</ServicesTitle>

            {user.services &&
              user.services.length > 0 &&
              user.services.map((item, key) => (
                <ServiceItem key={key}>
                  <ServiceLeft>
                    <ServiceName>{item.name}</ServiceName>
                    <ServicePrice>
                      {item.price.toLocaleString('pt-br', {
                        style: 'currency',
                        currency: 'BRL',
                      })}
                    </ServicePrice>
                  </ServiceLeft>
                  <ServiceRightBtn onPress={() => handleSelected(item)}>
                    <ServiceBtnText>Agendar</ServiceBtnText>
                  </ServiceRightBtn>
                </ServiceItem>
              ))}
          </ServiceArea>
          {user.comments && user.comments.length > 0 && (
            <CommentArea>
              <Swiper
                style={{ height: 120 }}
                showsPagination={false}
                showsButtons={true}
                prevButton={<PrevSvg width="35" height="35" fill="#000000" />}
                nextButton={<NextSvg width="35" height="35" fill="#000000" />}
              >
                {user.comments.map((item, key) => (
                  <CommentItem key={key}>
                    <CommentInfo>
                      <CommentName>{item.name}</CommentName>
                      <Stars stars={item.rate} showNumber={false} />
                    </CommentInfo>
                    <CommentBodyText>{item.body}</CommentBodyText>
                  </CommentItem>
                ))}
              </Swiper>
            </CommentArea>
          )}
        </BodyArea>
      </Scroller>
      <Back onPress={() => navigation.goBack()}>
        <BackSvg width="44" height="44" fill="#FFFFFF" />
      </Back>
      {modal && (
        <ModalBaber
          show={modal}
          setShow={setModal}
          barber={user}
          service={serviceSeleted}
        />
      )}

      {loading && <Loading />}
    </Container>
  );
}
