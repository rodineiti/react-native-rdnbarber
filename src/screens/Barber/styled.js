import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  background-color: #ffffff;
  flex: 1;
`;

export const Scroller = styled.ScrollView`
  flex: 1; // pega do o espaço da tela de cima pra baixo
`;

export const SwipeDot = styled.View`
  width: 10px;
  height: 10px;
  background-color: ${(props) => props.bgColor || '#ffffff'};
  border-radius: 5px;
  margin: 3px;
`;

export const SwipeItem = styled.View`
  flex: 1;
  background-color: #63c2d1;
`;

export const SwipeImage = styled.Image`
  width: 100%;
  height: 240px;
`;

export const FakeSwiper = styled.View`
  height: 140px;
  background-color: #63c2d1;
`;

export const BodyArea = styled.View`
  background-color: #ffffff;
  border-top-left-radius: 50px;
  margin-top: -50px;
  min-height: 400px;
`;

export const UserArea = styled.View`
  flex-direction: row;
  margin-top: -30px;
`;

export const UserAvatar = styled.Image`
  width: 110px;
  height: 110px;
  border-radius: 20px;
  margin-left: 30px;
  margin-right: 20px;
  border-width: 4px;
  border-color: #ffffff;
  background-color: #e7e7e7;
`;

export const UserInfo = styled.View`
  flex: 1;
  justify-content: flex-end;
`;

export const UserName = styled.Text`
  color: #000000;
  font-size: 18px;
  font-weight: bold;
  margin-bottom: 10px;
`;

export const UserFavBtn = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  background-color: #ffffff;
  border: 2px solid #999999;
  border-radius: 20px;
  justify-content: center;
  align-items: center;
  margin-right: 20px;
  margin-left: 20px;
  margin-top: 20px;
`;

export const ServiceArea = styled.View`
  margin-top: 30px;
`;

export const ServicesTitle = styled.Text`
  font-size: 18px;
  font-weight: bold;
  color: #268596;
  margin-left: 30px;
  margin-bottom: 20px;
`;

export const ServiceItem = styled.View`
  flex-direction: row;
  margin-right: 30px;
  margin-left: 30px;
  margin-bottom: 20px;
`;

export const ServiceLeft = styled.View`
  flex: 1;
`;

export const ServiceName = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: #268596;
`;

export const ServicePrice = styled.Text`
  font-size: 14px;
  color: #268596;
`;

export const ServiceRightBtn = styled.TouchableOpacity`
  background-color: #4eadbe;
  border-radius: 10px;
  padding: 10px 15px;
`;

export const ServiceBtnText = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: #ffffff;
`;

export const CommentArea = styled.View`
  margin-top: 30px;
  margin-bottom: 50px;
`;

export const CommentItem = styled.View`
  background-color: #268596;
  padding: 15px;
  border-radius: 10px;
  height: 120px;
  justify-content: center;
  margin-left: 40px;
  margin-right: 40px;
`;

export const CommentInfo = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
`;

export const CommentName = styled.Text`
  color: #ffffff;
  font-size: 14px;
  font-weight: bold;
`;

export const CommentBodyText = styled.Text`
  color: #ffffff;
  font-size: 13px;
`;

export const Back = styled.TouchableOpacity`
  position: absolute;
  left: 0px;
  top: 15px;
  z-index: 9;
`;
